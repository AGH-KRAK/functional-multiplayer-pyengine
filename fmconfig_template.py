settings = {

    #  == File Locations
    # 'build path':   '../build/',


    # == Server Config
    # 'server host':  socket.gethostname(),
    # 'command port': 21701,
    # 'game port':    21702,
    # 'config port':  21703,


    # == Constants
    # 'update rate': 5,
    # 'max cmd size': 1024,


    # == Handshake with Server
    # 'request': handshake.Request(),
    # 'on accept': lambda accept: None,
    # 'on reject': lambda reject: None,


    # == Methods
    # 'client init': lambda: None,
    # 'render': lambda game: None,
    # 'validate': lambda command: command,


    # == Engine Configuration
    # 'start game': lambda: None,
    # 'run every frame': lambda func: None
}