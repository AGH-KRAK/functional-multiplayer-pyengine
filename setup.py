from setuptools import setup

setup(
    name="functional-multiplayer",
    version='0.1',
    description='A python implementation of the engine-side of the Functional Multiplayer project',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Game Development',
        'License :: GPL'
        ],
    python_requires='~=3.0',

    packages=['src/functional_multiplayer'],

    options={
        'build_apps': {
            'gui_apps': {
                'client': 'src/client.py',
                'panda_instance': 'src/panda_instance.py',
            },
            'macos_main_app': 'client',
            'platforms': [
                'manylinux1_x86_64',
                # 'macosx_10_6_x86_64',
                # 'win32',
                # 'win_amd64',
            ],

        },
    },
)
