from config import settings
import networking as net
import sys

_cmd_port = settings['command port']

cmd_socket = net.make_socket(net.UDP, _cmd_port)

while True:
    cmd, (client_address, _) = cmd_socket.recvfrom(settings['max cmd size'])
    # todo: validate command, validate or parse client address
    print(cmd.hex())
    sys.stdout.flush()
