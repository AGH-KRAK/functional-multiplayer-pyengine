import os
import sys
import socket
import proto
import handshake_pb2 as handshake
cwd = os.getcwd()
parent_cwd = os.path.abspath(os.path.join(cwd, os.pardir))
grandpar_cwd = os.path.abspath(os.path.join(parent_cwd, os.pardir))
sys.path += [cwd, parent_cwd, grandpar_cwd]

try:
    import fmconfig
    _fmsettings = fmconfig.settings
except ImportError:
    fmconfig = ...
    print("Warning: No file named fmconfig.py found."
          "Using default configuration.", file=sys.stderr)
    _fmsettings = {}


_default_settings = {
    # file locations
    'build path':   '../build/',
    # server config
    'server host':  socket.gethostname(),
    'command port': 21701,
    'game port':    21702,
    'config port':  21703,
    # server handshake
    'request':   handshake.Request(),
    'on accept': lambda accept: None,
    'on reject': lambda reject: None,
    # methods
    'client init':     lambda: None,
    'render':          lambda game: None,
    'validate':        lambda command: command,
    # engine-specific configuration
    'start game':      lambda: None,
    'run every frame': lambda func: None,
    # constants
    'update rate': 5,
    'max cmd size': 1024,
}


class _Settings:
    def __getitem__(self, *args):
        try:
            return _fmsettings.__getitem__(*args)
        except KeyError:
            return _default_settings.__getitem__(*args)


settings = _Settings()
