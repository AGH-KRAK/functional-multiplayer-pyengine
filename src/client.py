import atexit
from time import time

from config import settings
from subprocess import Popen, PIPE
import proto_wrapper as protobuf
import sys


def current_time() -> str:
    """Returns a unique string to identify an instance in game-time."""
    # Note: we may want to calculate the server tick + offset in ms
    return str(time())


def try_render_gamestate():
    """Communicate with the pseudoserver.

    Request a relevant gamestate we can show to the user."""
    # test if the pseudoserver is still alive.
    # fail if it isn't.
    ps_ret = pseudo_server.poll()
    if ps_ret is not None:
        raise ConnectionError("Lost Pseudoserver. Returned code " + str(ps_ret))

    # request the pseudoserver calculates our gamestate
    pseudo_server.stdin.write(current_time()+"\n")
    pseudo_server.stdin.flush()

    # get the gamestate from the pseudoserver
    game_hex = pseudo_server.stdout.readline().strip()

    # parse it with protobuf
    game = protobuf.parse_game(bytes.fromhex(game_hex))

    # and render it
    settings['render'](game)


if __name__ == "__main__":
    print("running client")
    # Initialize the pseudoserver as a subprocess
    pseudo_server = Popen(settings['build path'] + 'pseudoserver',
                          stdin=PIPE, stdout=PIPE,
                          universal_newlines=True)
    atexit.register(pseudo_server.kill)

    # Send a request to the server to be allowed into the game
    request_hex = protobuf.serialize_request(settings['request']).hex()
    pseudo_server.stdin.write(request_hex+"\n")
    pseudo_server.stdin.flush()

    print("sent request", settings['request'])

    response = protobuf.parse_response(
        bytes.fromhex(pseudo_server.stdout.readline().strip()))

    response_type = response.WhichOneof("response")
    if response_type is None:
        print("no response")
    elif response_type == "reject":
        print("rejected")
        settings['on reject'](response.reject)
    elif response_type == "accept":
        print("accepted")
        settings['on accept'](response.accept)

        # Initialize the game
        settings['client init']()

        settings['run every frame'](try_render_gamestate)

        # Start the game
        settings['start game']()
    else:
        print("unknown response type " + response_type, file=sys.stderr)
