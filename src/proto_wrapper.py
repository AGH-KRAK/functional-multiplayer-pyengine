# noinspection PyUnresolvedReferences
import proto
import sync_pb2
import command_pb2
import game_pb2
import handshake_pb2


def parse_game(wire_mess: bytes):
    game = game_pb2.Game()
    game.ParseFromString(wire_mess)
    return game


def parse_sync(wire_mess: bytes):
    sync = sync_pb2.Sync()
    sync.ParseFromString(wire_mess)
    return sync


def parse_response(wire_mess: bytes):
    response = handshake_pb2.Response()
    response.ParseFromString(wire_mess)
    return response


def serialize_sync(sync):
    assert isinstance(sync, sync_pb2.Sync)
    return sync.SerializeToString()


def serialize_request(request):
    assert isinstance(request, handshake_pb2.Request)
    return request.SerializeToString()


def create_sync_command(tick, command):
    sync = sync_pb2.Sync()
    sync.tick_id = tick
    sync.command.CopyFrom(command)
    return sync


def create_sync_game(tick, game):
    sync = sync_pb2.Sync()
    sync.tick_id = tick
    sync.game.CopyFrom(game)
    return sync


def create_example_sync(tick):
    cmd = command_pb2.Command()
    cmd.player_id = 1
    cmd.used_object = 4
    return create_sync_command(tick, cmd)
