import socket
from functools import partial
from typing import Tuple, Any, Union

TCP = socket.SOCK_STREAM
UDP = socket.SOCK_DGRAM

NUL = bytes.fromhex("00")


class Socket(socket.socket):
    def send(self, data: bytes, flags: int = 0) -> int:
        return super().send(data + NUL, flags)

    def sendto(self, data: bytes, address: Union[tuple, str]) -> int:
        return super().sendto(data + NUL, address)

    def recv(self, bufsize: int, flags: int = 0) -> bytes:
        return super().recv(bufsize, flags)[:-1]

    def recvfrom(self, bufsize: int, flags: int = 0) -> Tuple[bytes, Any]:
        data, addr = super().recvfrom(bufsize, flags)
        return data[:-1], addr


def make_socket(socket_type, port: int, listen_to=0) -> Socket:
    """
    Creates and binds a socket (for serverside use)
    Can also listen the socket.

    :param socket_type: TCP or UDP
    :param port
    :param listen_to: the max number of clients to listen for
        Ignored on non-TCP sockets or if 0.
    :return: a socket:
        If TCP, call accept() first, then send()/recv()
        If UDP, use sendto() and recvfrom()
    """
    sock = Socket(socket.AF_INET, socket_type)
    host = socket.gethostname()
    sock.bind((host, port))
    if listen_to > 0 and socket_type == TCP:
        sock.listen(listen_to)
    return sock


def connect_socket(socket_type, host_address: str, port: int) -> Socket:
    """
    Connects to a remote socket (for clientside use)

    :param socket_type: use TCP or UDP
    :param host_address
    :param port
    :return: a socket, ready for send()/recv()
    """
    _, _, _, _, sock_address = socket.getaddrinfo(
        host_address, port,
        socket.AF_INET, socket_type)[0]

    host_ip = sock_address[0]
    sock = Socket(socket.AF_INET, socket_type)
    sock.connect((host_ip, port))
    return sock
