import proto
from command_pb2 import Command
import networking as net
import proto_wrapper

cmd_socket = None


def initialize_socket():
    global cmd_socket
    if cmd_socket is not None:
        return
    from config import settings
    cmd_socket = net.connect_socket(net.UDP,
                                    settings['server host'],
                                    settings['command port'])


def send_command(tick_id: int, command: Command):
    if cmd_socket is None:
        initialize_socket()
    sync = proto_wrapper.create_sync_command(tick_id, command)
    wire_mess = proto_wrapper.serialize_sync(sync)
    cmd_socket.send(wire_mess)
