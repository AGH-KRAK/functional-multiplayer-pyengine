#!/bin/bash

export SRC_DIR=../../schema/ DST_DIR=./src/proto
if [[ $OS == 'Windows_NT' ]]; then
	../protoc -I=$SRC_DIR --python_out=$DST_DIR $SRC_DIR/*.proto
else
	protoc -I=$SRC_DIR --python_out=$DST_DIR $SRC_DIR/*.proto
fi